TA_pipeworks = {}

TA_pipeworks.modpath = minetest.get_modpath("pipeworks") .. "/ta"

dofile(TA_pipeworks.modpath .. "/default_settings.lua")

if TA_pipeworks.settings.replace_metal_sounds_on_wood_sounds then
    dofile(TA_pipeworks.modpath .. "/replace sounds.lua")
end

if TA_pipeworks.settings.remove_some_handlers_from_straight_pipe then
    dofile(TA_pipeworks.modpath .. "/remove_handlers.lua")
end

if TA_pipeworks.settings.use_own_recipes then
    dofile(TA_pipeworks.modpath .. "/use_own_recipes.lua")
end

if TA_pipeworks.settings.ignore_items_in_deployer then
    dofile(TA_pipeworks.modpath .. "/ignore_items_in_deployer.lua")
end

if TA_pipeworks.settings.ignore_items_in_tubes then
    dofile(TA_pipeworks.modpath .. "/ignore_items_in_tubes.lua")
end
if pipeworks.enable_item_tags and TA_pipeworks.settings.tags_sorting_tube then
    dofile(TA_pipeworks.modpath .. "/tags_sorting_tube.lua")
end
