local groups_to_skip = {
    'bag',
}

local search_group_in_table = function(item_name)
    for _, value in ipairs(groups_to_skip) do
        if minetest.get_item_group(item_name, value) ~= 0 then
            return true
        end
    end
    return false
end

local original_handler = pipeworks.tube_inject_item
pipeworks.tube_inject_item = function(pos, start_pos, velocity, item, owner)
    local stack = ItemStack(item)
    local name = item.get_name and item:get_name()
    name = name or "air"
    if search_group_in_table(name) then
        minetest.add_item(pos, stack:to_string())
        minetest.chat_send_player(owner,
            "One or more of your items was ejected. Items in group:bag cannot go through pipeworks anymore.")
        return
    end

    original_handler(pos, start_pos, velocity, item, owner)
end

TA_pipeworks.items_should_not_be_vacuumed = function(lua_entity)
    local item = ItemStack(lua_entity.itemstring)
    if not item then return false end
    return search_group_in_table(item:get_name())
end
