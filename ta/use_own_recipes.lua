local recipes_to_replace = {}

local materials = {
    stone = "default:stone",
    steel_ingot = "default:steel_ingot",
    mese_crystal = "default:mese_crystal",
    plastic_sheet = "basic_materials:plastic_sheet",
    group_wood = "group:wood",
    group_smoothstone = "group:smoothstone",
    chest = "default:chest",
    adamantite_ingot = "xtraores:adamantite_ingot",
    osmium_ingot = "xtraores:osmium_ingot",
}

table.insert(recipes_to_replace, {
    name = "pipeworks:filter",
    unregister = {
        { { materials.steel_ingot, materials.steel_ingot, materials.plastic_sheet },
            { "group:stick",         materials.mese_crystal, materials.plastic_sheet },
            { materials.steel_ingot, materials.steel_ingot,  materials.plastic_sheet }
        },
    },
    recipes = {
        {
            { materials.steel_ingot, materials.steel_ingot,   materials.plastic_sheet },
            { "group:stick",         "underch:saphire_block", materials.plastic_sheet },
            { materials.steel_ingot, materials.steel_ingot,   materials.plastic_sheet },
        }
    }
})

if pipeworks.enable_node_breaker then
    table.insert(recipes_to_replace, {
        name = "pipeworks:nodebreaker_off",
        unregister = {
            { { "basic_materials:gear_steel", "basic_materials:gear_steel", "basic_materials:gear_steel" },
                { materials.stone,              "mesecons:piston",            materials.stone },
                { materials.group_wood,         "mesecons:mesecon",           materials.group_wood },
            },
        },
        recipes = {
            {
                { materials.osmium_ingot,     materials.osmium_ingot, materials.osmium_ingot },
                { materials.adamantite_ingot, "mesecons:piston",      materials.adamantite_ingot },
                { materials.group_wood,       "mesecons:mesecon",     materials.group_wood },
            }
        }
    })
end

if pipeworks.enable_deployer then
    table.insert(recipes_to_replace, {
        name = "pipeworks:deployer_off",
        unregister = {
            {
                { materials.group_wood, materials.chest,    materials.group_wood },
                { materials.stone,      "mesecons:piston",  materials.stone },
                { materials.stone,      "mesecons:mesecon", materials.stone },
            },
        },
        recipes = {
            {
                { materials.group_wood,       materials.chest,    materials.group_wood },
                { materials.adamantite_ingot, "mesecons:piston",  materials.adamantite_ingot },
                { materials.stone,            "mesecons:mesecon", materials.stone },

            },
            {
                { materials.group_wood,        materials.chest,    materials.group_wood },
                { materials.adamantite_ingot,  "mesecons:piston",  materials.adamantite_ingot },
                { materials.group_smoothstone, "mesecons:mesecon", materials.group_smoothstone },
            }
        }
    })
end

for _, replacement_info in ipairs(recipes_to_replace) do
    for _, unregister_recipe in ipairs(replacement_info.unregister or {}) do
        minetest.clear_craft({ recipe = unregister_recipe })
    end

    for _, recipe in ipairs(replacement_info.recipes or {}) do
        minetest.register_craft({
            output = replacement_info.name,
            recipe = recipe
        })
    end
end
