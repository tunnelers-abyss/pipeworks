local replace_song_nodes = {
    "pipeworks:valve_on_loaded",
    "pipeworks:grating",
    "pipeworks:spigot",
    "pipeworks:spigot_pouring",
    "pipeworks:entry_panel_empty",
    "pipeworks:entry_panel_loaded",
    "pipeworks:flow_sensor_empty",
    "pipeworks:flow_sensor_loaded",
    "pipeworks:fountainhead",
    "pipeworks:fountainhead_pouring",
    "pipeworks:straight_pipe_empty",
    "pipeworks:straight_pipe_loaded",
}

local states = { "on", "off" }
for s in ipairs(states) do
    table.insert(replace_song_nodes, "pipeworks:pump_" .. states[s])
    table.insert(replace_song_nodes, "pipeworks:valve_" .. states[s] .. "_empty")
end

for fill = 0, 10 do
    table.insert(replace_song_nodes, "pipeworks:expansion_tank_" .. fill)
    table.insert(replace_song_nodes, "pipeworks:storage_tank_" .. fill)
end

for _, node_name in ipairs(replace_song_nodes) do
    local node_def = minetest.registered_nodes[node_name]
    if node_def then
        minetest.override_item(node_name, {
            sounds = default.node_sound_wood_defaults()
        })
    end
end
