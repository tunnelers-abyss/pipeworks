local prefix = "TA_pipeworks_"

local settings = {
    replace_metal_sounds_on_wood_sounds = false,
    remove_some_handlers_from_straight_pipe = false,
    use_own_recipes = true,
    ignore_items_in_deployer = true,
    ignore_items_in_tubes = true,
    respect_old_nodebreacker_behavior = true,
    tags_sorting_tube = true,
}

TA_pipeworks.settings = {}

for name, value in pairs(settings) do
    local setting_type = type(value)
    if setting_type == "boolean" then
        TA_pipeworks.settings[name] = minetest.settings:get_bool(prefix .. name, value)
    elseif setting_type == "number" then
        TA_pipeworks.settings[name] = tonumber(minetest.settings:get(prefix .. name) or value)
    else
        TA_pipeworks.settings[name] = value
    end
end
