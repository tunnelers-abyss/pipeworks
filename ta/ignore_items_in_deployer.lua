if pipeworks.enable_deployer then
    local items_to_reject = {
        { item = "^farming:trellis$" },
        { item = "^driftcar:" },
        { item = "^bike:" },
        {
            item = "^farming:scythe",
            pointedTo = "^farming:grapes"
        },
    }

    TA_pipeworks.items_in_deployer_to_ignore = function(virtplayer, pointed_thing)
        local wieldstack = virtplayer:get_wielded_item()
        local wielded_item_name = wieldstack:get_name() or ""
        local pos = minetest.get_pointed_thing_position(pointed_thing)
        local nodename = minetest.get_node(pos).name

        for _, item_to_reject in ipairs(items_to_reject) do
            if string.match(wielded_item_name, item_to_reject.item) then
                if not item_to_reject.pointedTo then return true end

                if string.match(nodename, item_to_reject.pointedTo) then
                    return true
                end
            end
        end
        return false
    end
end
