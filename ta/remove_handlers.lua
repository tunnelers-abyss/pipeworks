local remove_handlers_nodes = {
    "pipeworks:straight_pipe_empty",
    "pipeworks:straight_pipe_loaded"

}

for _, node_name in ipairs(remove_handlers_nodes) do
    local node_def = minetest.registered_nodes[node_name]
    if node_def then
        node_def.on_rotate = nil
        node_def.check_for_pole = nil
        node_def.check_for_horiz_pole = nil
    end
end
