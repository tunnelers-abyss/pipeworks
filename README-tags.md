Welcome to Tags Pipeworks world!
===============================

I hope, this my addon will help to to construct more amazing setups...

Main idea is: to assign some tag to a stack of items into pipelines and to make decision about routing based on the tag.
In this case, Injectors should support tag assignment (as they are kind of'entrances' into pipelines) and filters should support tags (at least, LuaTube).
In additional, also was added  'Tags Sorting Pneumatic Tube Segment' which works like a usual 'Sorting Pneumatic Tube Segment', but operates with items tags instead of items. 
So in 'Tags Pipelines' world  doesn't matter what is the item, much important, which tag the item has.

Common rules for  tags in Injectors and LuaTubes
================================================

Commas in tags aren't allowed and will be replaced by undescore (_).
Leading and trailing spaces in tags will be trimmed.

Eg.  If you specify a tag like '      My ,,,,,  Tag       ' then it will be processed as 'My _____  Tag'


Maximum lenght of a tag is 30 (may be redefined via pipeworks_item_tag_name_limit)

Rules for 'Tags Sorting Pneumatic Tube Segment'
===============================================

If an item in the tube doesn't have a tag, but you want to sort it, then you should specify
special tag '`<<notag>>`' in one (or in few) of direction fields. If you don't specify the tag, the tube will works like usual sorting tube with empty directions. 

You may specify few tags for a direction into 'Tags Sorting Pneumatic Tube Segment', comma separated.
Eg:

'`    ToFurnance, <<notag>>   , ToStorage,          My ,,,  Tag    `'

it will be processed like the list:
- '`ToFurnance`'
- '`<<notag>>`'
- '`ToStorage`'
- '`My`'
- '`Tag`'

empty tag definitions are skipped.

Maximum lenght of one string with comma-separated tags is 30*6 = 180  (may be redefined via pipeworks_item_tag_name_limit, will be multiplied by 6)

Digiline Filter Injector
========================

Your LuaController now may send a command to an Injector with tag definition which should be assigned to injected stack.

Example:

```
digiline_send("MyInjector",
    {
       item="default:dirt",
       count = 99,
       tag = "SomeTag"
    }
)
```

In this case, even if your Digiline Injector already has a tag in a settings, the tag will be redefined by a tag from event message.
If you didn't specify a tag into event message, then predefined tag will be used. If you didn't set predefined tag, then no any tags will be assigned to items.

LuaTube
=======

LuaTubes may have an access to item tags via functions:

```
myVar = get_item_tag()

set_item_tag("MyTag")
```
